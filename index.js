module.exports = {
  "extends": "stylelint-config-recommended",
  "rules": {
    "at-rule-empty-line-before": null,
    "at-rule-no-unknown": [
      true,
      {
        "ignoreAtRules": [
          "if",
          "else",
          "mixin",
          "include",
          "content",
          "for",
          "extend",
          "import",
          "function"
        ]
      }
    ],
    "string-quotes": "double",
    "selector-max-specificity": "0,4,0",
    "selector-max-type": 0,
    "selector-max-id": 0,
    "no-extra-semicolons": true,
    "indentation": 4,
    "no-duplicate-selectors": true,
    "color-hex-case": "lower",
    "color-named": "never",
    "selector-no-qualifying-type": true,
    "selector-attribute-quotes": "always",
    "declaration-block-trailing-semicolon": "always",
    "function-url-quotes": "always"
  }
};
