# `@skoda-dms/stylelint-config`

> Developement [Stylelint](https://stylelint.io/) config.

## Usage

**Install**:

```bash
$  npm install --save-dev @skoda-dms/stylelint-config
```

**Edit `stylelintrc`**:

```jsonc
{
  // ...
  "extends": "@skoda-dms/stylelint-config"
}
```
